López Lara Marco Antonio
//Realizar una expresión regular que detecte emails correctos.
$patron='/^/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/'

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$patron='/^[^0-4][a-zA-Z0-9_]+[^0-9][^0-6]+[^0-4][a-zA-Z0-9_]+[^0-9][^0-2]$/'


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$patron='^[0-50][a-zA-Z0-9_]?$'

//Crea una funcion para escapar los simbolos especiales.
$patron='[ \ ^ $ . | ? * + ( )'

//Crear una expresion regular para detectar números decimales.
$patron='^[0-9]+([.][0-9]+)?$'